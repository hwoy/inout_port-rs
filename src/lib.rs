//! # inout_port-rs
//!
//! inb/outb port in freestanding Rust.
//!
//!
//! ## Example#1
//! ```
//! #![no_std]
//! #![no_main]
//!
//! extern crate inout_port_rs;
//!
//! #[no_mangle]
//! pub extern "C" fn _start() -> !
//! {
//! 	_ = main();
//! 	loop {}
//! }
//!
//! use core::panic::PanicInfo;
//! #[panic_handler]
//! fn panic(_info: &PanicInfo) -> ! {
//!     loop {}
//! }
//!
//! #[derive(Copy, Clone)]
//! #[repr(C, packed)]
//! struct __LowHigh {
//!     l: u8,
//!     h: u8,
//! }
//!
//! #[derive(Copy, Clone)]
//! union __Cursor {
//!     value: u16,
//!     lh: __LowHigh,
//! }
//! fn set_cursor(y: usize, x: usize) {
//!     let cursor = __Cursor {
//!         value: (y * 80 + x) as u16,
//!     };
//!     unsafe {
//!         inout_port_rs::outb(0xe, 0x3d4);
//!         inout_port_rs::outb(cursor.lh.h, 0x3d5);
//!         inout_port_rs::outb(0xf, 0x3d4);
//!         inout_port_rs::outb(cursor.lh.l, 0x3d5);
//!     }
//! }
//! fn main() {
//! 	set_cursor(0,0);
//! }
//! ```
//!

#![no_std]

#[cfg(all(not(target_arch = "x86"), not(target_arch = "x86_64")))]
compile_error!("Arch was not supported.");

use core::arch::asm;

/// Write a one byte data to port
pub unsafe fn outb(value: u8, port: u16) {
    asm!("outb %al, %dx" ,
    in("dx") (port),
    in("al") (value),
    options(att_syntax)
    );
}

/// Read a one byte data from port
pub unsafe fn inb(port: u16) -> u8 {
    let value: u8;

    asm!("inb %dx, %al" ,
    in("dx") (port),
    out("al") (value),
    options(att_syntax)
    );
    value
}
