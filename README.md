# inout_port-rs

inb/outb port in freestanding Rust.

## How to use

```sh
cargo add inout_port-rs

```

## Example

```rust
#![no_std]
#![no_main]

extern crate inout_port_rs;

#[no_mangle]
pub extern "C" fn _start() -> !
{
	_ = main();
	loop {}
}

use core::panic::PanicInfo;
#[panic_handler]
fn panic(_info: &PanicInfo) -> ! {
    loop {}
}

#[derive(Copy, Clone)]
#[repr(C, packed)]
struct __LowHigh {
    l: u8,
    h: u8,
}

#[derive(Copy, Clone)]
union __Cursor {
    value: u16,
    lh: __LowHigh,
}
fn set_cursor(y: usize, x: usize) {
    let cursor = __Cursor {
        value: (y * 80 + x) as u16,
    };
    unsafe {
        inout_port_rs::outb(0xe, 0x3d4);
        inout_port_rs::outb(cursor.lh.h, 0x3d5);
        inout_port_rs::outb(0xf, 0x3d4);
        inout_port_rs::outb(cursor.lh.l, 0x3d5);
    }
}
fn main() {
	set_cursor(0,0);
}

```

## Contact me

- Web: <https://github.com/hwoy>
- Email: <mailto:bosskillerz@gmail.com>
- Facebook: <https://www.facebook.com/watt.duean>
